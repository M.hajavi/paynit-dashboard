import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './auth.service';

import { LoginComponent } from './login/login.component';
import { TemplateComponent } from './template/template.component';
import { CustomersComponent } from './customers/customers.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}
